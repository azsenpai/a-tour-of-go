// https://tour.golang.org/flowcontrol/5
package main

import (
	"fmt"
	"math"
)

func sqrt(x float64) string {
	if x < 0 {
		return sqrt(-x) + "i"
	}

	return fmt.Sprint(math.Sqrt(x))
}

func main() {
	a := float64(2)
	b := float64(-4)

	fmt.Printf("sqrt(%.2f) = %s\n", a, sqrt(a))
	fmt.Printf("sqrt(%.2f) = %s\n", b, sqrt(b))
}
