// https://tour.golang.org/flowcontrol/8
package main

import "fmt"

func Sqrt(x float64) float64 {
	z := 1.0

	for i := 0; i < 10; i++ {
		z -= (z*z - x) / (2 * z)
		fmt.Printf("Iteration %d: sqrt(%.2f) = %g\n", i, x, z)
	}

	return z
}

func main() {
	fmt.Println(Sqrt(2))
}
