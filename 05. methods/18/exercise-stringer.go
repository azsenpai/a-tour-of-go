// https://tour.golang.org/methods/18
package main

import "fmt"

type IPAddr [4]byte

func (a IPAddr) String() string {
	s := ""

	if len(a) > 0 {
		s += fmt.Sprintf("%d", a[0])
	}

	for i := 1; i < len(a); i++ {
		s += fmt.Sprintf(".%d", a[i])
	}

	return s
}

func main() {
	hosts := map[string]IPAddr{
		"loopback":  {127, 0, 0, 1},
		"googleDNS": {8, 8, 8, 8},
	}
	for name, ip := range hosts {
		fmt.Printf("%v: %v\n", name, ip)
	}
}
