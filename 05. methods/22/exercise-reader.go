// https://tour.golang.org/methods/22
package main

import "golang.org/x/tour/reader"

type MyReader struct{}

func (m MyReader) Read(b []byte) (n int, err error) {
	n = len(b)

	for i := 0; i < n; i++ {
		b[i] = 'A'
	}

	return
}

func main() {
	reader.Validate(MyReader{})
}
