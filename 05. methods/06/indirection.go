// https://tour.golang.org/methods/6
package main

import "fmt"

type Vertex struct {
	X, Y float64
}

func (v *Vertex) Scale(k float64) {
	v.X *= k
	v.Y *= k
}

func ScaleFunc(v *Vertex, k float64) {
	v.X *= k
	v.Y *= k
}

func main() {
	v := Vertex{3, 4}

	v.Scale(2)
	ScaleFunc(&v, 5)

	p := &Vertex{4, 3}

	p.Scale(10)
	ScaleFunc(p, 2)

	fmt.Println(v, p)
}
