// https://tour.golang.org/methods/23
package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func rot13(n byte) byte {
	var i byte

	if 'A' <= n && n <= 'Z' {
		i = byte('A')
	} else if 'a' <= n && n <= 'z' {
		i = byte('a')
	} else {
		return n
	}

	return (n - i + 13) % 26 + i
}

func (r rot13Reader) Read(b []byte) (n int, err error) {
	n, err = r.r.Read(b)

	for i := 0; i < n; i++ {
		b[i] = rot13(b[i])
	}

	return
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
