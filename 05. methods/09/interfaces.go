// https://tour.golang.org/methods/9
package main

import (
	"fmt"
	"math"
)

type Abser interface {
	Abs() float64
}

func main() {
	var a Abser

	f := MyFloat(-math.Sqrt2)
	v := Vertex{3, 4}

	a = f
	fmt.Printf("%#v\n", a)

	a = &v
	fmt.Printf("%#v\n", a)

	// a = v // error: Vertex does not implement Abser
}

type MyFloat float64

func (m MyFloat) Abs() float64 {
	if m < 0 {
		return float64(m)
	}

	return float64(m)
}

type Vertex struct {
	X, Y float64
}

func (v *Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}
