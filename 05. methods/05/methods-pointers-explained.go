// https://tour.golang.org/methods/5
package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

func Abs(v Vertex) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func Scale(v *Vertex, k float64) {
	v.X *= k
	v.Y *= k
}

func main() {
	v := Vertex{3, 4}
	Scale(&v, 10)

	fmt.Println(Abs(v))
}
