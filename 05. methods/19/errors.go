// https://tour.golang.org/methods/19
package main

import (
	"fmt"
	"time"
)

type MyType struct {
	When time.Time
	What string
}

func (m *MyType) Error() string {
	return fmt.Sprintf("at %v, %s", m.When, m.What)
}

func run() error {
	return &MyType{time.Now(), "it didn't work"}
}

func main() {
	if err := run(); err != nil {
		fmt.Println(err)
	}
}
