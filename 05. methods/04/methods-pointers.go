// https://tour.golang.org/methods/4
package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (v *Vertex) Scale(k float64) {
	v.X *= k
	v.Y *= k
}

func main() {
	v := Vertex{3, 4}
	v.Scale(10)

	fmt.Println(v.Abs())
}
