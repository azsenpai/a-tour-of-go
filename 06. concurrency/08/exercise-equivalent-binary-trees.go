package main

import (
	"fmt"
	"golang.org/x/tour/tree"
)

func RecWalk(t *tree.Tree, ch chan int) {
	if t == nil {
		return
	}
	RecWalk(t.Left, ch)
	ch <- t.Value
	RecWalk(t.Right, ch)
}

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan int) {
	RecWalk(t, ch)
	close(ch)
}

// Same determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *tree.Tree) bool {
	c1 := make(chan int)
	c2 := make(chan int)

	go Walk(t1, c1)
	go Walk(t2, c2)

	for {
		v1, ok1 := <-c1
		v2, ok2 := <-c2

		switch {
		case ok1 != ok2:
			return false
		case !ok1:
			return true
		case v1 != v2:
			return false
		}
	}
}

func main() {
	fmt.Println(Same(tree.New(1), tree.New(1)))
	fmt.Println(Same(tree.New(1), tree.New(2)))
}
