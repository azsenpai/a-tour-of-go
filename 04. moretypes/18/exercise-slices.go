// https://tour.golang.org/moretypes/18
package main

import "golang.org/x/tour/pic"

func Pic(dy, dx int) [][]uint8 {
	d := make([][]uint8, dy)

	for i := 0; i < len(d); i++ {
		d[i] = make([]uint8, dx)
	}

	for i := 0; i < dy; i++ {
		for j := 0; j < dx; j++ {
			d[i][j] = uint8(i ^ j)
		}
	}

	return d
}

func main() {
	pic.Show(Pic)
}
