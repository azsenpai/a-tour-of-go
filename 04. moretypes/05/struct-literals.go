// https://tour.golang.org/moretypes/5
package main

import "fmt"

type Vertex struct {
	X, Y int
}

func main() {
	v0 := Vertex{}
	v1 := Vertex{1, 2}
	v2 := Vertex{Y: 3}

	p := &Vertex{X: 4, Y: 5}
	p.Y = 6

	fmt.Println(v0)
	fmt.Println(v1)
	fmt.Println(v2)
	fmt.Println(*p)
}
