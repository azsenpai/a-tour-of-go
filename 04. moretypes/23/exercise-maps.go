// https://tour.golang.org/moretypes/23
package main

import (
	"golang.org/x/tour/wc"
	"strings"
)

func WordCount(s string) map[string]int {
	c := make(map[string]int)

	for _, w := range strings.Fields(s) {
		c[w] += 1
	}

	return c
}

func main() {
	wc.Test(WordCount)
}
