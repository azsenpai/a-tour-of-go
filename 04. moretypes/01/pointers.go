// https://tour.golang.org/moretypes/1
package main

import "fmt"

func main() {
	i, j := 42, 2701

	p := &i
	fmt.Println(p)
	fmt.Println(*p)

	*p = 21
	fmt.Println(i)

	p = &j
	fmt.Println(p)
	fmt.Println(*p)

	*p /= 27
	fmt.Println(j)
}
