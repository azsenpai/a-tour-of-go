// https://tour.golang.org/moretypes/10
package main

import "fmt"

func main() {
	s := []int{2, 3, 5, 7, 11, 13}

	a := s[1:4]
	fmt.Println(a)

	s = s[:2]
	fmt.Println(s)

	s = s[1:]
	fmt.Println(s)

	fmt.Println(a)
}
