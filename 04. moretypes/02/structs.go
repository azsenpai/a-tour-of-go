// https://tour.golang.org/moretypes/2
package main

import "fmt"

type Vertex struct {
	X int
	Y int
}

func main() {
	v0 := Vertex{}
	v1 := Vertex{1, 2}
	v2 := Vertex{X: 3, Y: 4}
	v3 := Vertex{Y: 5, X: 6}

	fmt.Println(v0)
	fmt.Println(v1)
	fmt.Println(v2)
	fmt.Println(v3)
}
