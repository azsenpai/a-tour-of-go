// https://tour.golang.org/basics/7
package main

import "fmt"

func split(n int) (a, b int) {
	a = n / 10
    b = n % 10

	return
}

func main() {
	fmt.Println(split(17))
}
