// https://tour.golang.org/basics/14
package main

import "fmt"

func main() {
	v := 1 + 5i
	fmt.Printf("v is of type %T\n", v)
}
